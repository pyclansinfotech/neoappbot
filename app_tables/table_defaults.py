Table_defaults =[
{"Integer": ["quantity", "qty",  "number", "count",'phone']},
{"Boolean": ["status", "done",  "check"]},
{"Float": ["height", "weight",  "mass", "volume", "measurement", "size", "length", "depth"]},
{"Memo": ["body", "text",  "info"]},
{"Datetime": ["date", "time",  "datetime"]},
{"Currency": ["price", "cost", "total", "sub-total", "grand-total", "tax", "ex-tax", "inc-tax", "retail", "wholesale"]},
{'Char':["Name","Address","firstname","lastname"]}
]
