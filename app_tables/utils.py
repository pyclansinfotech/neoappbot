from django.db import models

def create_model(name, fields, module, options=None, admin_opts=None):
    """
    Create specified model
    """
    class Meta:
        # Using type('Meta', ...) gives a dictproxy error during model creation
        pass

    # if app_label:
    #     # app_label must be set using the Meta inner class
    #     setattr(Meta, 'app_label', 'app_tables')

    # # Update Meta with any options that were provided
    # if options is not None:
    #     for key, value in options.iteritems():
    #         setattr(Meta, key, value)

    # Set up a dictionary to simulate declarations within a class
    attrs = {'__module__': 'app_tables.models'}

    # Add in any fields that were provided
    if fields:
        attrs.update(fields)
        print 'attrs',attrs

    # Create the class, which automatically triggers ModelBase processing
    model = type(str(name), (models.Model,), attrs)

    # Create an Admin class if admin options were provided
    if admin_opts is not None:
        class Admin(admin.ModelAdmin):
            pass
        for key, value in admin_opts:
            setattr(Admin, key, value)
        admin.site.register(model, Admin)

    return model
# model=create_model('Person',app_label="app_tables",module="NeoAppBot.app_tables.models",fields="{'name':models.CharField(max_length=255)}")    
