# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AppTablesConfig(AppConfig):
    name = 'app_tables'
