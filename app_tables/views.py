# -*- coding: utf-8 -*-
# from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse
from django.views.generic import TemplateView
from .utils import create_model
from django.db import models
from django.views.decorators.csrf import csrf_exempt
from table_defaults import *
import os
from django.core.files import File
from django.core.cache import cache
from django.apps import apps
from .models import TableInfo
from django.utils.decorators import method_decorator
import json
from vanilla import ListView

# Create your views here.
class GeneratedModelBase(models.Model):
    class Meta:
        abstract = True
        app_label = 'app_tables'


"""Home Page"""
@method_decorator(csrf_exempt,name="dispatch")
class index(ListView):

	model = TableInfo
	
	def post(self,request):
		try:
			############ Save table details to create dynamic model out of it #######
			fields_dict={}
			fields_list=[]
			defaults = Table_defaults
			dict_data = json.loads(request.body)
			table_name = dict_data['table_name']
			table_fields = dict_data['fields']
			modal_tableinfo = TableInfo.objects.create(table_name=table_name,field_names=table_fields)
			modal_tableinfo.save()
			############  Now create dynamic model   #################
			fields_list = 	modal_tableinfo.field_names
			for i in range(0,len(table_fields)):
				for j in range(0,len(defaults)):
					for k,v in defaults[j].items():
						v = [x.upper() for x in v]
						if(fields_list[i].upper() in v):
							if(k == 'Char'):
								fields_dict.update({str(fields_list[i]):models.CharField(max_length=255)})
							elif(k == 'Integer'):
								fields_dict.update({str(fields_list[i]):models.IntegerField()})
			fields_dict.update({str('__module__'): str('app_tables.models')})
			model = type(str(table_name),  (GeneratedModelBase,),fields_dict)
			os.system("python manage.py makemigrations")
			os.system("python manage.py migrate")
			return HttpResponse('ok')					
		except Exception as e:
			print 'error',e	
		

			
	

	







			








	

