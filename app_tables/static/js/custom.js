/Controller for Homepage*/
$(document).ready(function() {
	var count = 0;

	$(document).keypress(function(e) {
	    if(e.which === 13){
	      count++;

	      if(count === 2) { 
	        count = 0; 
	        var data  = $('#model_data').val();
	        console.log('data>>',data)
	        data_content = data.split(/\n/)
	        console.log('data',data_content);
	        var table_name_str  = data_content[0].split(':')
	        var table_name = table_name_str[1]
	        var fields_array = []
	        for(var i=1; i<=data_content.length; i++){
	        	if(data_content[i] != undefined){
		        	if(data_content[i].length>0){
		        		if(data_content[i].indexOf(' ')> -1){
		        		var field_name  = data_content[i].split(' ')
		        		fields_array.push(field_name[1])
		        	}
		        	else{
		        		var field_name  = data_content[i].split('>')
		        		fields_array.push(field_name[1])
		        	}
		        		
		        	}
		        }	
	        }
	        console.log('table',table_name);
	        console.log('fields[]',fields_array);
	        params = {
	        	'table_name':table_name,
	        	'fields':fields_array,
	        }
	        $.ajax({
	            type: "POST",
	            url: "/",
	            data: JSON.stringify(params),
	           success: function (response) {
           		if(response == 'ok'){
           			window.location.reload();
           		}               

		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log('error')   
		        }
        	});
	        return;
	      }
	    }
	});
});